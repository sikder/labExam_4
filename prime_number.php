<?php
$start = 9;
$end = 100;
$count = 1;
$fp = fopen("prime.txt","w+");

for($i=$start;$i<=$end;$i++){
    $num = $i;
    if($num == 1)
        continue;
        $isPrime = true;
        for($j=2;$j<=ceil($num/2);$j++){
            if($num % $j == 0) {
                $isPrime = false;
                break;
            }
        }
    if($isPrime){
        echo "Prime Number : ".$num."<br>";
        fwrite($fp,"$num ");
        if($count == 10){
            fwrite($fp,"\n");
            $count = 0;
        }
        $count++;
    }
}

fclose($fp);
?>